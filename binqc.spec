/*
A KBase module: binqc
This tool labels bins as high, medium, or low quality based on their
completeness(checkM), contamination(checkM), number of tRNAs(infernal),
and ribosomal RNAs(infernal)
*/

module binqc {
    /* An X/Y/Z style reference
    */
    typedef string obj_ref;
    
    /*
        Reference to an assembly object
        @id ws KBaseGenomeAnnotations.Assembly
    */
    typedef string assembly_ref;
    
    /*
        A 'typedef' can also be used to define compound or container
        objects, like lists, maps, and structures.  The standard KBase
        convention is to use structures, as shown here, to define the
        input and output of your function.  Here the input is a
        reference to the binned contigs data object and a workspace to save
        output.

        To define lists and maps, use a syntax similar to C++ templates
        to indicate the type contained in the list or map.  For example:

            list <string> list_of_strings;
            mapping <string, int> map_of_ints;
    */
    typedef structure {
        obj_ref binned_contig_obj_ref;
        string workspace_name;
    } BinQCParams;


    /*
        Here is the definition of the output of the function.  The output
        can be used by other SDK modules which call your code, or the output
        visualizations in the Narrative.  'report_name' and 'report_ref' are
        special output fields- if defined, the Narrative can automatically
        render your Report.
    */
    typedef structure {
        string report_name;
        string report_ref;
    } BinQCResults;
    
    /*
        The actual function is declared using 'funcdef' to specify the name
        and input/return arguments to the function.  For all typical KBase
        Apps that run in the Narrative, your function should have the 
        'authentication required' modifier.
    */
    funcdef bin_qc(BinQCParams params)
        returns (BinQCResults output) authentication required;
};
