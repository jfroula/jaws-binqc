FROM continuumio/miniconda

# dependency versions used at time of development
# pplacer==1.1.alpha19
# infernal==1.1 (cmsearch)
# prodigal==2.6.3
# hmmer==3.1b1
# checkm==1.0.11
# trnascan-se==1.3.1
# refinem==

RUN mkdir -p /binqc/bin
WORKDIR /binqc

#
# Install conda dependent trnascan-se and refinem
#
RUN conda install -y -c bioconda trnascan-se==2.0
RUN conda install -y -c bioconda bbmap==38.22

# install refineM (this has issues because instaling with pip
# makes refinem not see pysam libs that exist in miniconda/pkgs
#RUN conda install -y -c bioconda krona
#RUN ktUpdateTaxonomy.sh /root/miniconda2/opt/krona/taxonomy
#RUN pip install refinem

# add checkm and dependencies to PATH
ENV CHECKM_VERSION=1.0.11

#
# Install apt-get dependencies
#
RUN apt-get update && \
    apt-get install -y hmmer prodigal unzip infernal

#
# Install pplacer
#
RUN wget "https://github.com/matsen/pplacer/releases/download/v1.1.alpha19/pplacer-linux-v1.1.alpha19.zip" && \
    unzip pplacer-linux-v1.1.alpha19.zip && \ 
    rm -f pplacer-linux-v1.1.alpha19.zip && \
    mv pplacer-Linux-v1.1.alpha19 /binqc/bin/pplacer

#
# Install checkm
#
RUN conda install -y -c bioconda pysam==0.15.0
RUN apt-get install -y libbz2-dev liblzma-dev gcc
RUN pip install checkm-genome==1.0.8 

# For checkm-genome required data
RUN mkdir -p /data/checkm_data 

#
# Download checkm data
# these commands to download checkm data was already done on dnt03.nersc.gov.
#	cd /data/checkm_data && \
#	curl -L -O https://data.ace.uq.edu.au/public/CheckM_databases/checkm_data_2015_01_16.tar.gz && \
#	tar xzf checkm_data_2015_01_16.tar.gz && \
#	rm checkm_data_2015_01_16.tar.gz
#
RUN echo /global/dna/shared/databases/DSI/checkm_data | checkm data setRoot /global/dna/shared/databases/DSI/checkm_data

# all PERL modules and MetaQC pm's for metaQc.pl
COPY metaqc /binqc/metaqc

ENV PATH "$PATH:/binqc/bin/pplacer"
ENV PATH "$PATH:/binqc/metaqc"
ENV PERL5LIB "/binqc/metaqc/lib:/opt/conda/lib/tRNAscan-SE:$PERL5LIB"

ENTRYPOINT [ "./scripts/entrypoint.sh" ]
CMD [ ]
