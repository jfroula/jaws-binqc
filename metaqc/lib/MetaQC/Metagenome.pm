=pod

=head1 NAME

MetaQC::Assembly

=head1 DESCRIPTION

Class for representing/organizing a metagenome assembly.

=head1 METHODS

=over 5

=cut

package MetaQC::Metagenome;

use strict;
use Env qw(TMPDIR);
use JSON;
use File::Slurp;
use File::Which;
use File::Copy;
use File::Path qw(make_path remove_tree);
use File::Spec;
use constant { TRUE => 1, FALSE => 0};
use MetaQC::RRNA;
use MetaQC::TRNA;
use MetaQC::CheckM;
use MetaQC::Bins;
#use MetaQC::TaxFilter;
use Parallel::ForkManager;

our $json = new JSON;

=item new

Constructor

=cut

sub new
{
	my ($class, $configFile, $dir, $name, $contigsFile, $covFile, $threads, $ram, $metabatIndir, $cmsearchInfile, $tRnascanInfile) = @_;
	$configFile or die("Config file required\n");
	$dir or die("Outdir required\n");
	$name or die("Assembly name required\n");
	$contigsFile or die("Contigs file required\n");
	$threads or die("Threads required\n");
	print "Using $threads threads\n";
	$dir = File::Spec->rel2abs($dir);
	$contigsFile = File::Spec->rel2abs($contigsFile);
	$configFile = File::Spec->rel2abs($configFile);
	$covFile = File::Spec->rel2abs($covFile);

	-d $dir or make_path($dir) or die("Unable to mkdir $dir: $!\n");
	my $binsDir = "$dir/bins";
	-d $binsDir or make_path($binsDir) or die($!);
	$contigsFile eq "$dir/contigs.fna" or symlink($contigsFile, "$dir/contigs.fna");
	#if ( $covFile )
	#{
	#	$covFile eq "$dir/coverage.metabat.tsv" or copy($covFile, "$dir/coverage.metabat.tsv");
	#}
	my $proteinsFile = "$dir/proteins.faa";
	my $annotFile = "$dir/protVsNr.tsv";

	my $txt = read_file($configFile);
	my $config = from_json($txt);

	my $this =
	{
		bins => undef,
		bestDir => undef,
		config => $config,
		contigsFile => $contigsFile,
		dir => $dir,
		binsDir => $binsDir,
		name => $name,
		covFile => $covFile,
		threads => $threads,
		ram => $ram,
		proteinsFile => $proteinsFile,
		annotFile => $annotFile
	};
	bless $this, $class;

	# INIT
	print "Begin processing metagenome $name\n";
	my $rRna = $this->{rRna} = MetaQC::RRNA->new($config, $dir, $contigsFile, $threads, $cmsearchInfile);
	my $tRna = $this->{tRna} = new MetaQC::TRNA($config, $dir, $contigsFile, $threads, $tRnascanInfile);
	my $checkm = $this->{checkm} = new MetaQC::CheckM($config, "$dir/checkm", $threads, $ram);
#	my $taxFilter = $this->{taxFilter} = new MetaQC::TaxFilter($config, $dir, $contigsFile, $threads);

	# BIN
	my $parameterSetsHR = $this->{config}->{BIN}->{exe};
	my $bestScore = -1;
	my $bestSrcDir;
	my $numParameterSets = scalar(keys %$parameterSetsHR);
	foreach my $paramsName (keys %$parameterSetsHR)
	{
		print "Processing: $paramsName\n";
		my $paramsCmd = $parameterSetsHR->{$paramsName};
		my $bins = $this->{bins} = new MetaQC::Bins($this->{config}, $this->{dir}, $this->{binsDir}, $this->{name}, $this->{contigsFile}, $this->{covFile}, $this->{rRna}, $this->{tRna}, $this->{checkm}, $this->{taxFilter}, $this->{threads}, $paramsName, $paramsCmd, $metabatIndir);
		print "Calculating score\n";
		my $score = $bins->score;
		print "$paramsName score = $score\n";
		if ( $score > $bestScore )
		{
			$bestScore = $score;
			$this->{bins} = $bins;
			$bestSrcDir = "$dir/bins/$paramsName";
		}
	}
	my $bestDestDir = "$dir/bins/best";
	-l $bestDestDir and unlink($bestDestDir);
	if ( $numParameterSets > 1 )
	{
		symlink($bestSrcDir, $bestDestDir) or die("Error symlinking $bestSrcDir $bestDestDir: $!\n");
	}
	return $this;
}

=item loadResults

Load previously generated bins.

=cut

sub loadResults
{
	my $this = shift;
	print "Checking ", $this->{binsDir}, " for previous binning results\n";
	my %results;
	opendir(DIR, $this->{binsDir}) or die($!);
	my @dirs = grep { /^metabat[12]:.*$/ } readdir(DIR);
	closedir(DIR);
	my $best;
	foreach my $dir (@dirs)
	{
		my $bins = new MetaQC::Bins($this->{config}, $this->{dir}, $this->{binsDir}, $this->{name}, $this->{contigsFile}, $this->{covFile}, $this->{rRna}, $this->{tRna}, $this->{checkm}, $this->{taxFilter}, $this->{threads}, { initFromId => $dir }); 
		$results{$dir} = $bins;
	}
	$this->{results} = \%results;
}

=item summary

Output summary table.

=cut

sub summary
{
	my ($this, $outfile) = @_;
	exists($this->{results}) or return;
	my $output = "#dir\tscore\n";
	foreach my $dir ( sort keys %{$this->{results}} )
	{
		my $bins = $this->{results}->{$dir};
		$output .= join("\t", $dir, $bins->score)."\n";
	}
	if ($outfile)
	{
		open(my $out, '>', $outfile) or die($!);
		print $out $output;
		close($out);
	}
	else
	{
		print $output;
	}
}

1;

=back

=head1 AUTHOR

Edward Kirton (ESKirton@LBL.gov)

=head1 COPYRIGHT/LICENSE

Copyright 2017 US DOE Joint Genome Institute.  Use freely under the same terms as Perl itself.

=head1 AUSPICE STATEMENT

The work conducted by the U.S. Department of Energy Joint Genome Institute is supported by the Office of Science of the U.S. Department of Energy under Contract No. DE-AC02-05CH11231.

=cut
