=pod

=head1 NAME

MetaQC::Bins

=head1 DESCRIPTION

Object representing one binner/parameter set applied to a metagenome assembly.

=head1 METHODS

=over 5

=cut

package MetaQC::Bins;

use strict;
use File::Path qw(make_path remove_tree);
use File::Copy;
use Env qw(TMPDIR SLURM_TMPDIR NERSC_HOST);

=item new

Constructor.  Requires output folder and contigs FASTA.  Optionally accepts metabat bins (if already exists).

=cut

sub new
{
	my ($class, $config, $rootDir, $parentDir, $name, $contigsFile, $covFileM, $rRna, $tRna, $checkm, $taxFilter, $threads, $paramsName, $paramsCmd, $importDir) = @_;
	defined($config) or die("config hashref required\n");
	ref($config) eq 'HASH' or die("Config hashref required\n");
	$parentDir or die("Parent dir required\n");
	$name or die("Assembly name required\n");
	$contigsFile or die("Contigs file required\n");
	$threads or die("Threads required\n");
	$paramsName or die("Clustering parameters name required\n");

	# NEW OBJ
	my $this =
	{
		config => $config,
		name => $name,
		parentDir => $parentDir,
		contigsFile => $contigsFile,
		covFileM => $covFileM, # coverage infile formatted for metabat
		covFileR => undef, # coverage infile formatted for refinem
		tetraFile => undef, # tnf infile for refinem
		threads => $threads,
		rRna => $rRna,
		tRna => $tRna,
		checkm => $checkm,
		taxFilter => $taxFilter,
		rootDir => $rootDir,
		dir => undef, # product of whichever filters have been specified in config
		bins => {},   # product of whichever filters have been specified in config
		metabatDir => undef, # MetaBAT output
		refinemDir => undef, # RefineM output; defined only if RefineM is used
		paramsName => $paramsName,
		paramsCmd => $paramsCmd,
		importDir => $importDir
	};
	bless $this, $class;

	# INITIALIZATION
	my $dir = $this->{metabatDir} = "$this->{parentDir}/$paramsName";
	print "Init new binning obj => $paramsName\n";
	$this->metabat;
	return $this;
}


=item runMetabat

Run metabat

=cut

sub metabat
{
	my $this = shift;

	# CHECK FOLDER FOR BINS
	my $dir = $this->{dir} = $this->{metabatDir};
	-d $dir or make_path($dir) or die("Unable to mkdir $dir: $!\n");
	opendir(DIR, $dir) or die($!);
	my @files = grep { /\d+\.fn?a$/ } readdir(DIR);
	closedir(DIR);

	# METABAT
	if ( @files )
	{
		print "Using existing binning results at $dir\n";
	} elsif ( $this->{paramsName} =~ /^external$/ )
	{
		my $srcDir = $this->{importDir};
		opendir(DIR, $srcDir) or die("External bins folder is empty: $srcDir\n");
		my @srcFiles = sort grep {/\.(fa|fna|fasta)$/i} readdir(DIR);
		closedir(DIR);
		my $numSrc = scalar(@srcFiles);
		print "Importing $numSrc bins from $srcDir\n";
		my $binNum=0;
		open(my $out, '>', "$dir/binNames.txt") or die($!);
		foreach my $src (@srcFiles)
		{
			++$binNum;
			my $dest = "bin.$binNum.fa";
			push @files, $dest;
			print "\tCopy $src -> $dest\n";
			print $out "$src\t$dest\n";
			copy("$srcDir/$src", "$dir/$dest") or die("Error copying $src -> $dest: $!\n");
		}
		close($out);
	} elsif ( !$this->{paramsCmd} )
	{
		die("Parameters command not defined for ".$this->{paramsName}." and no FASTA files found\n");
	} elsif ( $this->{covFileM} )
	{
		my $cmd = $this->{paramsCmd}." -i $this->{contigsFile} --cvExt $this->{covFileM} -o $dir/$this->{name} -t $this->{threads}";
		system($cmd) == 0 or die("ERROR: metabat failed: $!\n$cmd\n");
	} else
	{
		my $cmd = $this->{paramsCmd}." -i $this->{contigsFile} -o $dir/$this->{name} -t $this->{threads}";
		system($cmd) == 0 or die("ERROR: metabat failed: $!\n$cmd\n");
	}

	# UNBINNED LARGE CONTIGS BELONG IN THEIR OWN BIN
	$this->moveUnbinnedLargeContigs;

	# CONTINUE TO REFINEM OR DONE
	$this->loadBins;
	$this->annotate;
	$this->outputSummary;
	$this->{config}->{BIN}->{refineM} and $this->refinem;
}

=item moveUnbinnedLargeContigs

Move unbinned large contigs into their own bins.

=cut

sub moveUnbinnedLargeContigs
{
	my $this = shift;
	exists($this->{config}->{BIN}->{largeContigSize}) or return;
	my $minLargeContigSize = $this->{config}->{BIN}->{largeContigSize};
	$minLargeContigSize > 0 or return;

	# CHECK FOR LARGE CONTIGS FILE
	my $dir = $this->{metabatDir};
	my $largeContigsFile = "$dir/largeContigs.txt";
	-e $largeContigsFile and return; # already done
	print "Checking for large unbinned contigs\n";
	my $output = '';

	# LOAD ALL CONTIG IDS
	my $lastBinId = 0;
	my %contigs;
	opendir(DIR, $dir) or die($!);
	my @files = grep { /\d+\.fn?a$/ } readdir(DIR);
	closedir(DIR);
	foreach my $file (@files)
	{
		$file =~ /(\d+)\.fn?a$/ or die;
		$1 > $lastBinId and $lastBinId = $1;
		open(my $in, '<', "$dir/$file") or die($!);
		while (<$in>)
		{
			chomp;
			/^>(\S+)/ and $contigs{$1} = undef;
		}
		close($in);
	}

	# FIND LARGE CONTIGS
	my $contigId;
	my $fasta = '';
	my $len = 0;
	open(my $in, '<', $this->{contigsFile}) or die($!);
	while (<$in>)
	{
		if (/^>(\S+)/)
		{
			my $tmpFasta = $_;
			my $tmpContigId = $1;
			if (defined($contigId) and $len >= $minLargeContigSize)
			{
				# OUTPUT LARGE CONTIG
				++$lastBinId;
				my $binId = "$this->{name}.$lastBinId";
				my $file = "$dir/$this->{name}.$lastBinId.fa";
				print "Writing large contig $contigId to $file\n";
				open(my $out, '>', $file) or die($!);
				print $out $fasta;
				close($out);
				$output .= "$contigId\t$len\n";
			}

			# IS THE NEXT CONTIG UNBINNED?
			if ( exists($contigs{$tmpContigId}) )
			{
				$contigId = undef;
				$fasta = undef;
			} else
			{
				$fasta = $tmpFasta;
				$contigId = $tmpContigId;
			}
			$len = 0;
		} elsif ( $contigId )
		{
			$fasta .= $_;
			$len += length($_) - 1;
		}
	}
	close($in);
	if (defined($contigId) and $len >= $minLargeContigSize)
	{
		# OUTPUT LARGE CONTIG
		++$lastBinId;
		my $binId = "$this->{name}.$lastBinId";
		my $file = "$dir/$this->{name}.$lastBinId.fa";
		print "Writing large contig $contigId to $file\n";
		open(my $out, '>', $file) or die($!);
		print $out $fasta;
		close($out);
		$output .= "$contigId\t$len\n";
	}

	# WRITE OUTPUT
	open(my $out, '>', $largeContigsFile) or die($!);
	print $out $output;
	close($out);
}

=item coverage

Add contig length to coverage table

=cut

sub coverage
{
	my $this = shift;
	$this->{config}->{BIN}->{refineM} or return;

	my $infile = $this->{covFileM};
	-f $infile or die("Cannot run RefineM without coverage file\n");
	my $outfile = $this->{covFileR} = $this->{rootDir}.'/coverage.refinem.tsv';

	my $numContigs = 0;
	open(my $in, '<', $this->{contigsFile}) or die("Unable to read contigs file, ", $this->{contigsFile}, ": ", $!);
	while (<$in>) { /^>/ and ++$numContigs }
	close($in);
	$this->{numContigs} = $numContigs;

	if ( -e $outfile )
	{
		my $numIn = 0;
		open(my $in, '<', $infile) or die($!);
		while (<$in>) { ++$numIn }
		close($in);
		if ( $numIn < $numContigs )
		{
			warn("WARNING: There are only $numIn coverage values but $numContigs contigs\n");
		}

		my $numOut = 0;
		open(my $out, '<', $outfile) or die($!);
		while (<$out>) { ++$numOut }
		close($out);
		--$numOut; # header

		if ( $numIn == $numOut or $numOut == $numContigs )
		{
			print "Using existing RefineM coverage file\n";
			return;
		} else
		{
			warn("Purging incomplete RefineM coverage file\n");
			unlink($outfile);
		}
	}

	# DETERMINE CONTIG LENGTHS
	print "Determining contig lengths and adding to coverage file\n";
	my %contigs;
	my $contig;
	my $bp=0;
	open(my $fasta, '<', $this->{contigsFile}) or die($!);
	while (<$fasta>)
	{
		chomp;
		if (/^>(\S+)/)
		{
			$bp and $contigs{$contig} = $bp;
			$contig=$1;
			$bp=0;
		} else
		{
			$bp += length($_);
		}
	}
	close($fasta);
	$bp and $contigs{$contig} = $bp;

	# ADD CONTIG LENGTHS TO COVERAGE FILE
	open(my $in, '<', $infile) or die($!);
	open(my $out, '>', "$outfile.tmp") or die($!);
	print $out "Scaffold Id\tSequence length(bp)\tCoverage\n";
	while (<$in>)
	{
		chomp;
		my ($contig, $cov) = split(/\t/);
		$cov = int($cov+0.5); # round to whole number
		if ( ! exists($contigs{$contig}) )
		{
			warn("Contig coverage not defined for $contig\n");
			next;
		}
		my $len = $contigs{$contig};
		print $out join("\t", $contig, $len, $cov), "\n";
	}
	close($in);
	close($out);
	move("$outfile.tmp", $outfile);
}

=item tetra

Generate TNF table

=cut

sub tetra
{
	my $this = shift;
	$this->{config}->{BIN}->{refineM} or return;
	my $tetraFile = $this->{tetraFile} = $this->{rootDir}.'/tetra.tsv';

	if ( -e $tetraFile )
	{
		my $numContigs;
		if ( $this->{numContigs} )
		{
			$numContigs = $this->{numContigs};
		} else
		{
			open(my $in, '<', $this->{contigsFile}) or die($!);
			while (<$in>) { /^>/ and ++$numContigs }
			close($in);
			$this->{numContigs} = $numContigs;
		}

		my $numLines = 0;
		open(my $in, '<', $tetraFile) or die($!);
		while (<$in>) { ++$numLines }
		close($in);
		$numLines--; # header

		if ( $numContigs == $numLines )
		{
			print "Using existing TNF file\n";
			return;
		} else
		{
			unlink($tetraFile);
		}
	}

	# GENERATE TETRANUCLEOTIDE FREQUENCY TABLE
	print "Generating TNF file\n";
	if ( -d '/dev/shm' )
	{
		$this->_tetraHack;
	} else
	{
		my $cmd = "checkm tetra -q -t $this->{threads} $this->{contigsFile} $tetraFile.tmp 2>/dev/null >/dev/null";
		system($cmd) == 0 or die("ERROR checkm tetra failed: $!\n$cmd\n");
		move("$tetraFile.tmp", $tetraFile);
	}
}

=item _tetraHack

Calculate tetranucleotide counts for batches of contigs, since checkm tetra sometimes chokes on large files.

=cut

sub _tetraHack
{
	my $this = shift;
	my $contigsFile = $this->{contigsFile};
	my $tetraFile = $this->{tetraFile};

	my $hdr = "Sequence Id	AAAA	AAAC	AAAG	AAAT	AACA	AACC	AACG	AACT	AAGA	AAGC	AAGG	AAGT	AATA	AATC	AATG	AATT	ACAA	ACAC	ACAG	ACAT	ACCA	ACCC	ACCG	ACCT	ACGA	ACGC	ACGG	ACGT	ACTA	ACTC	ACTG	AGAA	AGAC	AGAG	AGAT	AGCA	AGCC	AGCG	AGCT	AGGA	AGGC	AGGG	AGTA	AGTC	AGTG	ATAA	ATAC	ATAG	ATAT	ATCA	ATCC	ATCG	ATGA	ATGC	ATGG	ATTA	ATTC	ATTG	CAAA	CAAC	CAAG	CACA	CACC	CACG	CAGA	CAGC	CAGG	CATA	CATC	CATG	CCAA	CCAC	CCAG	CCCA	CCCC	CCCG	CCGA	CCGC	CCGG	CCTA	CCTC	CGAA	CGAC	CGAG	CGCA	CGCC	CGCG	CGGA	CGGC	CGTA	CGTC	CTAA	CTAC	CTAG	CTCA	CTCC	CTGA	CTGC	CTTA	CTTC	GAAA	GAAC	GACA	GACC	GAGA	GAGC	GATA	GATC	GCAA	GCAC	GCCA	GCCC	GCGA	GCGC	GCTA	GGAA	GGAC	GGCA	GGCC	GGGA	GGTA	GTAA	GTAC	GTCA	GTGA	GTTA	TAAA	TACA	TAGA	TATA	TCAA	TCCA	TCGA	TGAA	TGCA	TTAA\n";

	my $tmpContigsFile = "/dev/shm/$$.contigs.fna";
	my $tmpTetraFile = "/dev/shm/$$.tetra.tsv";
	my $contigsPerBatch = 10000;

	my $fasta;
	my $n = 0;
	open(my $in, '<', $contigsFile) or die($!);
	open(my $out, '>', "$tetraFile.tmp") or die($!);
	print $out $hdr;
	while (<$in>)
	{
		if (/^>(\S+)/)
		{
			my $tmpFasta = $_;
			if ( ++$n > $contigsPerBatch )
			{
				open(my $tmp, '>', $tmpContigsFile) or die($!);
				print $tmp $fasta;
				close($tmp);
				my $cmd = "checkm tetra -q -t $this->{threads} $tmpContigsFile $tmpTetraFile 2>/dev/null >/dev/null";
				system($cmd) == 0 or die("FAILURE: $cmd\n$!\n");
				open($tmp, '<', $tmpTetraFile) or die($!);
				my $foo = <$tmp>; # discard header
				while (<$tmp>)
				{
					print $out $_;
				}
				close($tmp);
				$fasta = '';
				$n = 1;
			}
			$fasta .= $tmpFasta;
		} else
		{
			$fasta .= $_;
		}
	}
	close($in);
	if ( $n )
	{
		open(my $tmp, '>', $tmpContigsFile) or die($!);
		print $tmp $fasta;
		close($tmp);
		my $cmd = "checkm tetra -q -t $this->{threads} $tmpContigsFile $tmpTetraFile 2>/dev/null >/dev/null";
		system($cmd) == 0 or die("FAILURE: $cmd\n$!\n");
		open($tmp, '<', $tmpTetraFile) or die($!);
		my $foo = <$tmp>; # discard header
		while (<$tmp>)
		{
			print $out $_;
		}
		close($tmp);
	}
	close($out);
	unlink($tmpContigsFile, $tmpTetraFile);
	move("$tetraFile.tmp", $tetraFile);
}

=item scaffoldStats

Generate RefineM scaffold stats file.

=cut

sub scaffoldStats
{
	my $this = shift;
	my $cmd;
	my $scaffoldStatsFile = $this->{scaffoldStatsFile} = "$this->{metabatDir}/scaffold_stats.tsv";

	if ( -e $scaffoldStatsFile )
	{
		# VALIDATE EXISTING RESULTS
		my $numTetra = 0;
		open(my $in, '<', $this->{tetraFile}) or die($!);
		while (<$in>) { ++$numTetra }
		close($in);

		my $numStats = 0;
		open($in, '<', $scaffoldStatsFile) or die($!);
		my @row;
		while (<$in>)
		{
			++$numStats == 2 and @row = split(/\t/);
		}
		close($in);

		if ( $numTetra != $numStats )
		{
			warn("Purging incomplete scaffold stats file and downstream results\n");
			unlink($scaffoldStatsFile);
			remove_tree($this->{refinemDir});
		} elsif ( $row[3] == $row[4])
		{
			warn("Purging invalid scaffold stats file and downstream results\n");
			unlink($scaffoldStatsFile);
			remove_tree($this->{refinemDir});
		} else
		{
			print "Using existing scaffold stats file: $scaffoldStatsFile\n";
			return $scaffoldStatsFile;
		}
	}

	my $dir = $this->{metabatDir};
	-e $this->{tetraFile} or die("TNF file not found: ".$this->{tetraFile}."\n");
	unless ( $this->{covFileR} )
	{
		die("Misconfiguration error; coverage file required to use RefineM filter");
	}
	$cmd = "refinem scaffold_stats --silent -x fa --tetra $this->{tetraFile} --coverage_file $this->{covFileR} -c $this->{threads} $this->{contigsFile} $dir $dir 2>/dev/null >/dev/null";
	system($cmd) == 0 or die("RefineM scaffold stats failed:\n$cmd\n$!\n");
	return $scaffoldStatsFile;
}

=item refinem

Run RefineM to remove outlier contigs from bins.

=cut

sub refinem
{
	my $this = shift;
	$this->{config}->{BIN}->{refineM} or return;
	my $cmd;
	my $indir = $this->{metabatDir};
	my $outdir = $this->{dir} = $this->{refinemDir} = "$indir/filtered";
	-d $outdir or make_path($outdir) or die("Unable to mkdir $outdir: $!\n");

	# GENERATE STATISTICS FOR REFINEM OUTLIER DETECTION
	print "Begin RefineM\n";
	$this->coverage;
	$this->tetra;
	my $scaffoldStatsFile = $this->scaffoldStats;

	# IDENTIFY OUTLIERS
	print "MetaBAT results are filtered using RefineM\n";
	my $outfile = "$outdir/outliers.tsv";
	if ( ! -e $outfile )
	{
		$cmd = "refinem outliers --silent --no_plots $scaffoldStatsFile $outdir >/dev/null 2>/dev/null";
		system($cmd) == 0 or die("ERROR: refineM outliers failed: $!\n$cmd\n");
	}

	# CHECK IF BINS ALREADY PRESENT
	opendir(DIR, $outdir) or die($!);
	my @files = grep {/\d+\.fn?a$/} readdir(DIR);
	closedir(DIR);

	# FILTER BINS
	if ( ! scalar(@files) )
	{
		print "Writing refinem-filtered bins\n";

		# WRITE CHANGED BINS
		$cmd = "refinem filter_bins --silent --genome_ext fa --modified_only $indir $outfile $outdir >/dev/null 2>/dev/null";
		system($cmd) == 0 or die("ERROR: refineM filter_bins failed: $!\n$cmd\n");

		# SYMLINK UNCHANGED BINS
		opendir(DIR, $indir) or die($!);
		my @files = grep {/\d+\.fn?a$/} readdir(DIR);
		closedir(DIR);
		foreach my $file (@files)
		{
			$file =~ /^(.+\d+)\.fn?a$/ or die;
			my $binId = $1;
			my $infile = "$indir/$file";
			my $outfile = "$outdir/$file";
			-e $outfile and next;
			symlink($infile, $outfile) or die("Unable to symlink $infile -> $outfile: $!\n");
		}
	}

	# LOAD BINS
	$this->loadBins;
	$this->{config}->{BIN}->{taxFilter} and $this->taxFilter;
}

# NOT USED:
	#@cmd = qq(refinem genome_stats $refinemDir/scaffold_stats.tsv $refinemDir/genome_stats.tsv);
	#system(@cmd) == 0 or die("RefineM failed using command:\n@cmd\n$!\n");

=item taxFilter

Use TaxFilter to remove outlier contigs from bins.

=cut

sub taxFilter
{
	my $this = shift;
	$this->{config}->{BIN}->{taxFilter} or return;
	$this->{config}->{BIN}->{refineM} or die("taxFilter option without refineM is option unsupported\n");

	# DEFINE VARS
	my $indir = $this->{refinemDir};
	my $outdir = $this->{dir} = $this->{taxFilterDir} = "$indir/filtered";
	my $outliersFile = "$outdir/outliers.tsv";
	-d $outdir or make_path($outdir) or die("Unable to mkdir $outdir: $!\n");

	# CHECK IF DONE
	#-e $outliersFile and return $this->loadBins; # ECCE

	# GENERATE BIN LCA FILE AND IDENTIFY BINS TO FILTER (HIGH AND MED QUAL BINS ONLY)
	print "Filtering bins using TaxFilter\n";
	my %todo; # binId => undef
	foreach my $binId ( keys %{$this->{bins}} )
	{
		my $contigs = $this->{bins}->{$binId};
		my $fastaInfile = "$indir/$binId.fa";
		if ( ! -e $fastaInfile )
		{
			$fastaInfile = "$indir/$binId.fna";
			-e $fastaInfile or die("FASTA file not found for $binId in $indir\n");
		}
		my ($completeness, $contamination) = $this->{checkm}->stats($contigs, $fastaInfile);
		$completeness < 50 and next; # exclude low-quality bins
		$todo{$binId} = undef;
	}

	# IDENTIFY OUTLIER CONTIGS
	$this->{lca} = {};
	my $lcaFile = "$outdir/binLca.tsv";
	open(my $out, '>', $lcaFile) or die("Unable to write $lcaFile: $!\n");
	open(my $tab, '>', "$outliersFile.tmp") or die($!);
	foreach my $binId ( keys %{$this->{bins}} )
	{
		# LCA
		my $fastaInfile = "$indir/$binId.fa";
		my $fastaOutfile = "$outdir/$binId.fa";
		if ( ! -e $fastaInfile )
		{
			$fastaInfile = "$indir/$binId.fna";
			$fastaOutfile = "$outdir/$binId.fna";
			-e $fastaInfile or die("FASTA file not found for $binId in $indir\n");
		}
		-e $fastaOutfile and unlink($fastaOutfile);
		my ($lca, $filteredHR, $numFiltered) = $this->{taxFilter}->filter($this->{bins}->{$binId});
		$this->{lca}->{$binId} = $lca;
		print $out $binId, "\t", $lca, "\n";

		# TAX FILTER
		if ( exists($todo{$binId}) )
		{
			$numFiltered and print "$binId : $numFiltered contigs filtered\n";
			my @filtered = keys %$filteredHR;
			if ( $lca and $numFiltered )
			{
				# FILTER CONTIGS
				foreach my $contigId (@filtered)
				{
					print $tab join("\t", $binId, $lca, $contigId, $filteredHR->{$contigId}), "\n";
				}
				my $keep;
				open(my $in, '<', $fastaInfile) or die($!);
				open(my $out, '>', $fastaOutfile) or die($!);
				while (<$in>)
				{
					if (/^>(\S+)/)
					{
						$keep = exists($filteredHR->{$1}) ? 0:1;
					}
					if ( $keep )
					{
						print $out $_;
					}
				}
				close($in);
				close($out);
			} else
			{
				# NO CONTIGS REMOVED; SYMLINK FASTA
				symlink($fastaInfile, $fastaOutfile);
			}
		} else
		{
			# FILTER NOT APPLIED TO THIS BIN; SYMLINK FASTA
			symlink($fastaInfile, $fastaOutfile);
		}
	}
	close($tab);
	close($out);
	move("$outliersFile.tmp", $outliersFile);

	# LOAD BINS
	$this->loadBins;
	$this->annotate;
	$this->outputSummary;
}

=item loadBins

Read bins files and populate bins => contigs hashref.

=cut

sub loadBins
{
	my $this = shift;
	$this->{bins} = {};

	my $dir = $this->{dir};
	print "Loading bins in $dir\n";
	-d $dir or return;

	opendir(DIR, $dir) or die($!);
	my @files = grep { /\d+\.fn?a$/ } readdir(DIR);
	closedir(DIR);
	my $numBins = scalar(@files);
	if ( ! $numBins )
	{
		warn("No genome bins found in $dir\n");
		return;
	}

	# READ FASTA FILES
	my %contigs;
	foreach my $file (@files)
	{
		$file =~ /^(.+\d+)\.fn?a$/ or die;
		my $binId = $1;
		my @contigs;
		open(my $in, '<', "$dir/$file") or die($!);
		while (<$in>)
		{
			chomp;
			/^>(\S+)/ or next;
			my $contigId = $1;
			push @contigs, $contigId;
			if ( exists($contigs{$contigId}) )
			{
				die("Duplicate contig, $contigId, in $dir/$file and ".$contigs{$contigId}."\n");
			} else
			{
				$contigs{$contigId} = "$dir/$file";
			}
		}
		close($in);
		if ( exists($this->{bins}->{$binId}) )
		{
			die("Invalid bin filenames; there are multiple files matching bin ID: $binId\n");
		} else
		{
			$this->{bins}->{$binId} = \@contigs;
		}
	}
}

=item annotate

Annotate medium and high quality bin tRNAs

=cut

sub annotate
{
	my $this = shift;

	my $dir = $this->{dir};
	-d $dir or return;

	opendir(DIR, $dir) or die($!);
	my @files = grep { /\d+\.fn?a$/ } readdir(DIR);
	closedir(DIR);
	my $numBins = scalar(@files);
	$numBins or return;
	print "Annotating $numBins bins\n";

	# CHECKM
	$this->{checkm}->annotate($dir);

	my @tRnaQueueBacteria;
	my @tRnaQueueArchaea;
	foreach my $file (@files)
	{
		my @contigs;
		$file =~ /^(.+\d+)\.fn?a$/ or die;
		my $binId = $1;
		my $contigsFile = "$dir/$file";
		open(my $in, '<', $contigsFile) or die("Unable to read $contigsFile: $!");
		while (<$in>)
		{
			chomp;
			/^>(\S+)/ or next;
			push @contigs, $1;
		}
		close($in);
		my ($qual, $completeness, $contamination) = $this->{checkm}->qual(\@contigs, $contigsFile);
		my $domain = $this->{rRna}->domain(\@contigs);
		if ( $qual eq 'high' )
		{
			if ( $domain eq 'bacteria' ) { push @tRnaQueueBacteria, @contigs }
			elsif ( $domain eq 'archaea' ) { push @tRnaQueueArchaea, @contigs }
		}
	}

	# ANNOTATE TRNAS
	scalar(@tRnaQueueBacteria) and $this->{tRna}->search(\@tRnaQueueBacteria, 'bacteria');
	scalar(@tRnaQueueArchaea) and $this->{tRna}->search(\@tRnaQueueArchaea, 'archaea');
}

=item outputSummary

Writes table of bins and quality.

=cut

sub outputSummary
{
	my $this = shift;
	my $output = '#'.join("\t", qw(binId n_LSU n_SSU n_TSU domain n_tRNA completeness contamination qual n_contigs contig_bp gap_pct ctg_N50 ctg_L50 ctg_N90 ctg_L90 ctg_max gc_avg gc_std))."\n";
	foreach my $binId ( sort keys %{$this->{bins}} )
	{
		my $contigs = $this->{bins}->{$binId};
		my $contigsFile = "$this->{dir}/$binId.fa";
		if ( ! -e $contigsFile )
		{
			$contigsFile = "$this->{dir}/$binId.fna";
			-e $contigsFile or die("FASTA file not found for $binId in $this->{dir}\n");
		}
		my ($qual, $completeness, $contamination) = $this->{checkm}->qual($contigs, $contigsFile);
		my ($domain, $numLsu, $numSsu, $numTsu) = $this->{rRna}->domain($contigs);
		my ($tRnaComplete, $numTrnas) = $this->{tRna}->mostlyComplete($contigs);
		if ( $qual eq 'high' and ( ! $domain or ! $tRnaComplete ) )
		{
			$qual = 'medium';
		}
		my $stats = $this->_bbstats($contigsFile);
		$output .= join("\t", $binId, $numLsu, $numSsu, $numTsu, $domain, $numTrnas, $completeness, $contamination, $qual, @$stats)."\n";
	}
	my $outfile = "$this->{rootDir}/summary.tsv";
	open(my $out, '>', $outfile) or die($!);
	print $out $output;
	close($out);
	print "SUMMARY:\n", $output;
}

=item _bbstats

Run bbstats.sh to collect contig information.  Returns arrayref of: n_contigs, contig_bp, gap_pct, ctg_N50, ctg_L50, ctg_N90, ctg_L90, ctg_max, gc_avg, gc_std

=cut

sub _bbstats
{
	my ($this, $file) = @_;
	open(my $in, "cat $file | bbstats.sh format=5 in=stdin |") or die("Failed to run bbstats.sh on $file: $!\n");
	my $hdr = <$in>;
	my $row = <$in>;
	close($in);
	chomp $row;
	my @row = split(/\s+/, $row);
	return \@row;
}

=item score

Returns number of high-quality bins, considering rRNA, checkM, and tRNA.

=cut

sub score
{
	my $this = shift;
	my ($hq,$mq,$lq) = $this->scores;
	my $score = $hq + $mq/10;
	return $score;
}

=item scores

Returns number of high-quality bins, considering rRNA, tRNA, and CheckM completeness/contamination estimates.

=cut

sub scores
{
	my $this = shift;
	my $numHighQual = 0;
	my $numMedQual = 0;
	my $numLowQual = 0;
	foreach my $binId ( keys %{$this->{bins}} )
	{
		my $contigs = $this->{bins}->{$binId};
		my $binFile = "$this->{dir}/$binId.fa";
		if ( ! -e $binFile )
		{
			$binFile = "$this->{dir}/$binId.fna";
			-e $binFile or die("FASTA file not found for $binId in $this->{dir}\n");
		}
		my $qual = $this->{checkm}->qual($contigs, $binFile);
		my $domain = $this->{rRna}->domain($contigs);
		my $tRnaComplete = $this->{tRna}->mostlyComplete($contigs);
		if ( $qual eq 'high' )
		{
			if ( $domain and $tRnaComplete )
			{
				++$numHighQual;
			} else
			{
				$qual = 'medium';
				++$numMedQual;
			}
		} elsif ( $qual eq 'medium' )
		{
			++$numMedQual;
		} elsif ( $qual eq 'low' )
		{
			++$numLowQual;
		} else
		{
			die("Invalid bin quality was returned from checkm object for $binId\n");
		}
	}
	return ($numHighQual, $numMedQual, $numLowQual);
}

=item highQuality

Returns hashref of high quality bins.

=cut

sub highQuality
{
	my $this = shift;
	my %results;
	foreach my $binId ( keys %{$this->{bins}} )
	{
		my $contigs = $this->{bins}->{$binId};
		$this->{rRna}->domain($contigs) or next;
		my $binFile = "$this->{dir}/$binId.fa";
		if ( ! -e $binFile )
		{
			$binFile = "$this->{dir}/$binId.fna";
			-e $binFile or die("FASTA file not found for $binId in $this->{dir}\n");
		}
		$this->{checkm}->isHighQual($contigs, $binFile) or next;
		$this->{tRna}->mostlyComplete($contigs) or next;
		$results{$binId} = $contigs;
	}
	return \%results;
}

=item select

Returns hashref of selected bins.

=cut

sub select
{
	my ($this, $where) = @_;
	my %results;
	foreach my $binId ( keys %{$this->{bins}} )
	{
		my $contigs = $this->{bins}->{$binId};
		exists($where->{rRna}) and ! $this->{rRna}->domain($contigs) and next;
		exists($where->{tRna}) and ! $this->{tRna}->mostlyComplete($contigs) and next;
		my ($completeness, $contamination) = $this->{checkm}->stats($contigs);
		exists($where->{minCompleteness}) and $completeness < $where->{minCompleteness} and next;
		exists($where->{maxCompleteness}) and $completeness > $where->{maxCompleteness} and next;
		exists($where->{minContamination}) and $contamination < $where->{minContamination} and next;
		exists($where->{maxContamination}) and $contamination > $where->{maxContamination} and next;
		$results{$binId} = $contigs;
	}
	return \%results;
}


1;

=back

=head1 AUTHOR

Edward Kirton (ESKirton@LBL.gov)

=head1 COPYRIGHT/LICENSE

Copyright 2017 US DOE Joint Genome Institute.  Use freely under the same terms as Perl itself.

=head1 AUSPICE STATEMENT

The work conducted by the U.S. Department of Energy Joint Genome Institute is supported by the Office of Science of the U.S. Department of Energy under Contract No. DE-AC02-05CH11231.

=cut

__END__

sub getProteins
{
	my ($this, $dir) = @_;

	my $proteinsFile = "$dir/proteins.faa";
	my $covFile = "$dir/coverage.metabat.tsv";

	# DEFINE BIN DIR
	my $binDir;
	if ( -e $covFile )
	{
		$binDir = "$dir/bins/metabat1:m=3000,superspecific/filtered/filtered";
	} else
	{
		$binDir = "$dir/bins/metabat1:m=3000,superspecific";
	}
	unless ( -d $binDir )
	{
		warn("Folder not found: $binDir\n");
		return;
	}

	# CHEK IF DONE
	opendir(DIR, $binDir) or die($!);
	my @outfiles = sort grep {/^\S+\.\d+\.proteins\.faa$/} readdir(DIR);
	closedir(DIR);
	@outfiles and return;

	# LOAD PROTEINS
	print "Reading $proteinsFile\n";
	my $contigId;
	my $fasta = '';
	my %proteins;
	my $in;
	unless ( open($in, '<', $proteinsFile) )
	{
		warn("Unable to read $proteinsFile: $!\n");
		return;
	}
	while (my $line=<$in>)
	{
		if ( $line =~ /^>(\S+)_\d+/)
		{
			my $tmpContigId = $1;
			if ( $contigId ne $tmpContigId )
			{
				# WRITE PREVIOUS
				if ( defined($contigId) )
				{
					if ( exists($proteins{$contigId}) )
					{
						$proteins{$contigId} .= $fasta;
					} else
					{
						$proteins{$contigId} = $fasta;
					}
				}
				# INIT CURRENT
				$contigId = $tmpContigId;
				$fasta = '';
			}
		}
		$fasta .= $line;
	}
	close($in);
	if ( defined($contigId) )
	{
		if ( exists($proteins{$contigId}) )
		{
			$proteins{$contigId} .= $fasta;
		} else
		{
			$proteins{$contigId} = $fasta;
		}
	}

	# WRITE
	opendir(DIR, $binDir) or die($!);
	my @bins = sort grep {/^\S+\.\d+\.fa$/} readdir(DIR);
	closedir(DIR);
	foreach my $infile (@bins)
	{
		$infile =~ /^(\S+\.\d+)\.fa$/ or die;
		my $outfile = "$1.proteins.faa";
		print "\tReading $infile; Writing $outfile\n";
		open(my $in, '<', "$binDir/$infile") or die($!);
		open(my $out, '>', "$binDir/$outfile") or die($!);
		while (<$in>)
		{
			/^>(\S+)/ or next;
			my $contigId = $1;
			my $fasta = exists($proteins{$contigId}) ? $proteins{$contigId} : '';
			print $out $fasta;
			delete($proteins{$contigId});
		}
		close($in);
		close($out);
	}
}

sub visit
{
	my $dir = shift;
	my $proteinsFile = "$dir/proteins.faa";
	my $covFile = "$dir/coverage.metabat.tsv";

	# DEFINE BIN DIR
	my $binDir;
	if ( -e $covFile )
	{
		$binDir = "$dir/bins/metabat1:m=3000,superspecific/filtered/filtered";
	} else
	{
		$binDir = "$dir/bins/metabat1:m=3000,superspecific";
	}
	unless ( -d $binDir )
	{
		warn("Folder not found: $binDir\n");
		return;
	}

	# CHEK IF DONE
	opendir(DIR, $binDir) or die($!);
	my @outfiles = sort grep {/^\S+\.\d+\.proteins\.faa$/} readdir(DIR);
	closedir(DIR);
	@outfiles and return;

	# LOAD PROTEINS
	print "Reading $proteinsFile\n";
	my $contigId;
	my $fasta = '';
	my %proteins;
	my $in;
	unless ( open($in, '<', $proteinsFile) )
	{
		warn("Unable to read $proteinsFile: $!\n");
		return;
	}
	while (my $line=<$in>)
	{
		if ( $line =~ /^>(\S+)_\d+/)
		{
			my $tmpContigId = $1;
			if ( $contigId ne $tmpContigId )
			{
				# WRITE PREVIOUS
				if ( defined($contigId) )
				{
					if ( exists($proteins{$contigId}) )
					{
						$proteins{$contigId} .= $fasta;
					} else
					{
						$proteins{$contigId} = $fasta;
					}
				}
				# INIT CURRENT
				$contigId = $tmpContigId;
				$fasta = '';
			}
		}
		$fasta .= $line;
	}
	close($in);
	if ( defined($contigId) )
	{
		if ( exists($proteins{$contigId}) )
		{
			$proteins{$contigId} .= $fasta;
		} else
		{
			$proteins{$contigId} = $fasta;
		}
	}

	# WRITE
	opendir(DIR, $binDir) or die($!);
	my @bins = sort grep {/^\S+\.\d+\.fa$/} readdir(DIR);
	closedir(DIR);
	foreach my $infile (@bins)
	{
		$infile =~ /^(\S+\.\d+)\.fa$/ or die;
		my $outfile = "$1.proteins.faa";
		print "\tReading $infile; Writing $outfile\n";
		open(my $in, '<', "$binDir/$infile") or die($!);
		open(my $out, '>', "$binDir/$outfile") or die($!);
		while (<$in>)
		{
			/^>(\S+)/ or next;
			my $contigId = $1;
			my $fasta = exists($proteins{$contigId}) ? $proteins{$contigId} : '';
			print $out $fasta;
			delete($proteins{$contigId});
		}
		close($in);
		close($out);
	}
}

