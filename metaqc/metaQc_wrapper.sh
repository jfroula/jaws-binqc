#!/bin/bash 
# This script only creates a outdir and runs metaQc.pl in there so
# all tmp files will be there to be zipped by binqcUtil.py
BINS_DIR=$1
OUTDIR=$2
THREADS=$3

SCRIPT_DIR=$(cd "$(dirname "$(readlink -f "$0")")" && pwd)
BINS_DIR=$(readlink -f "$BINS_DIR")
OUTDIR=$(readlink -f "$OUTDIR")
THREADS=${THREADS:=32}
CONTIGS=$OUTDIR/contigs.fna

# threads shouldn't be less than 8
if [[ $THREADS < 8 ]]; then
	THREADS=8
fi

if [[ ! $BINS_DIR ]]; then
	echo "Usage: $0 <bins directory> <output directory>"
	exit 1
fi

# cd into outdir were all the temp and summary
# files are to be created

# delete old dir if exists because metaQC.pl will use old files if exist.
if [[ -d $OUTDIR ]]; then
	rm -rf $OUTDIR
fi

mkdir -p $OUTDIR
if [[ -d $OUTDIR ]]; then
	cd $OUTDIR
else
	echo "Directory $OUTDIR could not be created"
	exit 1
fi

# this makes sure fasta files all have same suffix. It aslo
# creates a concatenated fasta file called $concatenated_contigs
$SCRIPT_DIR/enforceFastaBin.pl $BINS_DIR $CONTIGS

echo "TMPDIR=$(pwd) perl $SCRIPT_DIR/metaQc.pl --mem 48G --config $SCRIPT_DIR/config.json --metabat $BINS_DIR --threads $THREADS --contigs $CONTIGS --out $OUTDIR"  
      TMPDIR=$(pwd) perl $SCRIPT_DIR/metaQc.pl --mem 48G --config $SCRIPT_DIR/config.json --metabat $BINS_DIR --threads $THREADS --contigs $CONTIGS --out $OUTDIR
	  
#rm -r $BINS_DIR/external $OUTDIR/checkm
